let job_data = {
  "fighter": {
    "specialty" : ["sword", "axe"],
    "atk_bonus" : 1500,
    "name": "ファイター"
  },
  "knight": {
    "specialty" : ["sword", "spear"],
    "atk_bonus" : 0,
    "name": "ナイト"
  },
  "priest": {
    "specialty" : ["stuff", "spear"],
    "atk_bonus" : 0,
    "name" : "プリースト"
  }
};

export default job_data;
